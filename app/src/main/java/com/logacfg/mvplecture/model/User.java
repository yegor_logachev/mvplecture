package com.logacfg.mvplecture.model;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by Yegor on 7/29/17.
 */

public class User {

    public static final String TABLE_NAME = "Users";
    private static final String ID = "_id";
    private static final String NAME = "name";
    private static final String SECOND_NAME = "second_name";
    private static final String PHONE = "phone";
    private static final String ADDRESS = "address";

    public static final String CREATE_SQL = "CREATE TABLE " + User.TABLE_NAME + " (" +
            User.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            User.NAME + " TEXT NOT NULL, " +
            User.SECOND_NAME + " TEXT, " +
            User.ADDRESS + " TEXT, " +
            User.PHONE + " TEXT);";

    public static final String[] COLUMNS = {User.NAME, User.SECOND_NAME, User.ADDRESS, User.PHONE};

    private String name;
    private String secondName;
    private String address;
    private String phone;

    public User(String name, String secondName, String address, String phone) {
        this.name = name;
        this.secondName = secondName;
        this.address = address;
        this.phone = phone;
    }

    public User(Cursor cursor) {
        int index = cursor.getColumnIndex(User.NAME);
        name = cursor.getString(index);
        index = cursor.getColumnIndex(User.SECOND_NAME);
        secondName = cursor.getString(index);
        index = cursor.getColumnIndex(User.ADDRESS);
        address = cursor.getString(index);
        index = cursor.getColumnIndex(User.PHONE);
        phone = cursor.getString(index);
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(User.NAME, name);
        values.put(User.SECOND_NAME, secondName);
        values.put(User.ADDRESS, address);
        values.put(User.PHONE, phone);
        return values;
    }

    @Override
    public String toString() {
        return name + " " + secondName + "\n" + address + "\n" + phone;
    }
}
