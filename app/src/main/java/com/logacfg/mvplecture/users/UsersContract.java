package com.logacfg.mvplecture.users;

import com.logacfg.mvplecture.BasePresenter;
import com.logacfg.mvplecture.BaseView;
import com.logacfg.mvplecture.model.User;

import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public interface UsersContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<Presenter> {

        void showUsers(List<User> users);

    }
}
