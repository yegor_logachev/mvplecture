package com.logacfg.mvplecture.users;

import android.content.Context;

import com.logacfg.mvplecture.data_source.DataSource;
import com.logacfg.mvplecture.data_source.DataSourceFabric;
import com.logacfg.mvplecture.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class UsersPresenter implements UsersContract.Presenter {

    private UsersContract.View view;
    private DataSource dataSource;

    public UsersPresenter(Context context, UsersContract.View view) {
        this.view = view;
        view.setPresenter(this);
        dataSource = DataSourceFabric.getInstance(context);
    }

    @Override
    public void start() {
        preInitDatabase();
        loadUsers();
    }

    private void loadUsers() {
        List<User> users = dataSource.loadUsers();
        if (users != null) {
            view.showUsers(users);
        }
    }

    private void preInitDatabase() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User("Vasyia " + i, "Petrov" + i,
                    "Sumska str. " + i, "112213123" + i);
            users.add(user);
        }
        dataSource.insertUsers(users);
    }
}
