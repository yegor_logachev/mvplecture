package com.logacfg.mvplecture.users;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.model.User;

import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class UsersFragment extends Fragment implements UsersContract.View {

    private UsersContract.Presenter presenter;
    private LinearLayout containerLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_users, container, false);
        containerLayout = rootView.findViewById(R.id.layoutUsers);
        return rootView;
    }

    @Override
    public void setPresenter(UsersContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.start();
    }

    @Override
    public void showUsers(List<User> users) {
        for (User user : users) {
            TextView textView = new TextView(getContext());
            textView.setText(user.toString());
            textView.setTextSize(30);
            LinearLayout.LayoutParams params =
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
            textView.setLayoutParams(params);
            containerLayout.addView(textView);
        }
    }
}
