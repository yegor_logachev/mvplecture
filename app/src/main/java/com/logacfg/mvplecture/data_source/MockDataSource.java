package com.logacfg.mvplecture.data_source;

import com.logacfg.mvplecture.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class MockDataSource implements DataSource {
    @Override
    public void insertUsers(List<User> users) {
        //// TODO: 7/29/17  imitate some action
    }

    @Override
    public List<User> loadUsers() {
        return new ArrayList<>(10);
    }
}
